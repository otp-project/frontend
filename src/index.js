import React from 'react';
import ReactDOM from 'react-dom/client';

import { createStore } from "redux";
import { Provider } from "react-redux";

import allReducers from './redux/reducers';

import { saveState, loadState } from "./redux/localstorage/localstorage";

import App from './App';

const presentedState = loadState();

let store = createStore(
  allReducers,
  presentedState,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
);

store.subscribe(() => saveState({
  session: store.getState().session,
}))

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <Provider store={store}>
      <App />
  </Provider>
);