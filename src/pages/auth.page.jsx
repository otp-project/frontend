import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import { useHistory } from "react-router-dom";

import {
    Container,
    Card,
    CardHeader,
    CardContent,
    TextField,
    Button,
    Dialog,
    DialogTitle,
    DialogContent,
    DialogActions,
    DialogContentText,
} from "@mui/material";

import Axios from "axios";

import { loginUser } from "../redux/actions/session";

import ShowSnackbar from "../components/snackbar.component";

const AuthenticationPage = () => {
    const history = useHistory();
    const dispatch = useDispatch();

    const session = useSelector(state => state.session);
    if (session) history.push("/home");

    const env = process.env;
    const baseUrl = env.REACT_APP_BACKEND_URL;

    const [phone, setPhone] = useState("");
    const [phoneError, setPhoneError] = useState(false);

    const [otp, setOtp] = useState("");
    const [otpError, setOtpError] = useState(false);

    const [otpDialog, setOtpDialog] = useState(false);

    const [loading, setLoading] = useState(false);
    const [openSnack, setOpenSnack] = useState(false);
    const [openSnackMessage, setOpenSnackMessage] = useState("");

    const sendOTP = () => {
        setLoading(true);
        setPhoneError(false);

        if (phone !== "") {
            Axios.post(`${baseUrl}/otp/generate`, { phone })
                .then((result) => {
                    setLoading(false);
                    setOpenSnack(true);
                    setOtpDialog(true);

                    setOpenSnackMessage(result.data.message);
                })
                .catch((error) => {
                    setLoading(false);
                    setOpenSnack(true);

                    setOpenSnackMessage(error.response.data.message);
                });
        } else {
            setLoading(false);
            setPhoneError(true);
        }
    }

    const authenticate = () => {
        setLoading(true);
        setOtpError(false);

        if (otp !== "") {
            Axios.post(`${baseUrl}/otp/check`, { phone, passcode: otp })
                .then((result) => {
                    dispatch(loginUser());

                    setLoading(false);
                    setOtpDialog(false);
                })
                .catch((error) => {
                    setLoading(false);
                    setOpenSnack(true);

                    setOpenSnackMessage(error.response.data.message);
                });
        } else {
            setLoading(false);
            setOtpError(true);
        }
    }

    return (
        <Container
            maxWidth="sm"
            sx={{
                height: "100vh",
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
        >
            <Card
                variant="elevation"
                elevation={20}
                sx={{
                    borderRadius: 1,
                    width: "100%"
                }}
            >
                <CardHeader
                    title="ورود با رمز یکبار مصرف"
                    sx={{
                        color: "secondary.main",
                        borderBottom: "solid",
                        borderBottomWidth: 1,
                        borderBottomColor: "primary.main",
                    }}
                />
                <CardContent>
                    <TextField
                        variant="outlined"
                        color="secondary"
                        label="شماره همراه"
                        placeholder="شماره همراه خود را وارد کنید. ۹۸۹۱۲xxxxxx"
                        margin="normal"
                        value={phone}
                        error={phoneError}
                        onChange={(e) => setPhone(e.target.value)}
                        autoComplete="off"
                        fullWidth
                    />
                    <Button
                        variant="contained"
                        size="large"
                        onClick={() => !loading && sendOTP()}
                        sx={{
                            mt: 2,
                        }}
                        disabled={loading && true}
                        disableElevation
                        fullWidth
                    >
                        { loading ? "لطفا صبر کنید" : "ارسال رمز" }
                    </Button>
                </CardContent>
            </Card>

            <Dialog
                open={otpDialog}
                sx={{
                    direction: "rtl",
                    textAlign: "right",
                }}
                maxWidth="sm"
                fullWidth
            >
                <DialogTitle
                    color="primary"
                    sx={{
                        color: "secondary.main",
                        borderBottom: "solid",
                        borderBottomWidth: 1,
                        borderBottomColor: "primary.main",
                    }}
                    gutterBottom
                >
                    احراز هویت
                </DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        یک کد ۶ رقمی برای شماره { phone } ارسال شده است. با وارد کردن آن کد در فیلد زیر، وارد حساب کاربری خود شوید.
                    </DialogContentText>
                    <TextField
                        variant="outlined"
                        color="secondary"
                        label="رمز یکبار مصرف"
                        placeholder="رمز یکبار مصرف ارسال شده را وارد کنید"
                        margin="normal"
                        value={otp}
                        error={otpError}
                        onChange={(e) => setOtp(e.target.value)}
                        fullWidth
                    />
                </DialogContent>
                <DialogActions>
                    <Button
                        variant="contained"
                        size="large"
                        color="error"
                        onClick={() => setOtpDialog(false)}
                        sx={{
                            ml: 1,
                            mb: 2,
                        }}
                        disabled={loading && true}
                        disableElevation
                    >
                        انصراف
                    </Button>
                    <Button
                        variant="contained"
                        size="large"
                        onClick={() => !loading && authenticate()}
                        sx={{
                            ml: 2,
                            mb: 2,
                        }}
                        disabled={loading && true}
                        disableElevation
                    >
                        { loading ? "لطفا صبر کنید" : "احراز هویت" }
                    </Button>
                </DialogActions>
            </Dialog>

            <ShowSnackbar
                open={openSnack}
                close={() => setOpenSnack(false)}
                message={openSnackMessage}
            />
        </Container>
    );
}

export default AuthenticationPage;