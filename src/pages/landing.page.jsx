import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

const LandingPage = () => {
    const history = useHistory();

    const session = useSelector(state => state.session);

    session ? history.push("/home") : history.push("/auth");
}

export default LandingPage;