import { combineReducers } from "redux";

import sessionReducer from "./session";

const allReducers = combineReducers({
    session: sessionReducer,
});

export default allReducers;