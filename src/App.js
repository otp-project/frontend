import {
  createTheme,
  CssBaseline,
  ThemeProvider,
  colors,
  Box,
} from "@mui/material";

import { useSelector } from "react-redux";

import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

import AuthenticationPage from "./pages/auth.page";
import HomePage from "./pages/home.page";
import LandingPage from "./pages/landing.page";

function App() {
  const mode = useSelector(state => state.theme);

  const theme = createTheme({
    palette: {
      mode: mode,
      background: {
        default: colors.orange[50],
      },
      primary: {
        main: colors.orange[800],
      },
      secondary: {
        main: "#000080",
      }
    },
    typography: {
      fontFamily: "Vazirmatn",
    },
  });
  
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Box
        sx={{
          direction: "rtl",
          textAlign: "right",
        }}
      >
        <Router>
          <Switch>
            <Route path={"/"} exact><LandingPage /></Route>
            <Route path={"/auth"} exact><AuthenticationPage /></Route>
            <Route path={"/home"} exact><HomePage /></Route>
          </Switch>
        </Router>
      </Box>
    </ThemeProvider>
  );
}

export default App;

